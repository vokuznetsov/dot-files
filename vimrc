set nocompatible
set encoding=UTF-8
syntax on

" -----------------------------------------------------------------------------
" Plugs
" -----------------------------------------------------------------------------
filetype off
call plug#begin('~/.vim/plugged')

Plug '/usr/local/opt/fzf'
Plug 'junegunn/fzf.vim'
Plug 'ervandew/supertab'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-endwise'
Plug 'Townk/vim-autoclose'
Plug 'scrooloose/nerdtree'
Plug 'sophacles/vim-bundle-sparkup'
Plug 'tomtom/tcomment_vim'
Plug 'sjl/gundo.vim'
Plug 'godlygeek/tabular'
Plug 'vim-scripts/L9'
Plug 'gmarik/snipmate.vim'
Plug 'krisleech/snipmate-snippets'
Plug 'kana/vim-textobj-user'
Plug 'pangloss/vim-javascript'
Plug 'thisivan/vim-bufexplorer'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'flazz/vim-colorschemes'
Plug 'ryanoasis/vim-devicons'
Plug 'wincent/terminus'
Plug 'tpope/vim-fireplace'
Plug 'guns/vim-clojure-highlight'
Plug 'jceb/vim-orgmode'
Plug 'shvechikov/vim-keymap-russian-jcukenmac'
Plug 'vim-syntastic/syntastic'
Plug 'tpope/vim-endwise'
Plug 'xavierd/clang_complete'

" Colorschemes
Plug 'morhetz/gruvbox'

" Elixir
Plug 'slashmili/alchemist.vim'
Plug 'elixir-editors/vim-elixir'

" Go
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'deoplete-plugins/deoplete-go', { 'do': 'make'}
let g:deoplete#sources#go#gocode_binary = '/Users/vawsome/go/bin/gocode'

" Rust
Plug 'rust-lang/rust.vim'
Plug 'racer-rust/vim-racer'
Plug 'sebastianmarkow/deoplete-rust'
let g:deoplete#sources#rust#racer_binary='/Users/vawsome/.cargo/bin/racer'
let g:deoplete#sources#rust#rust_source_path='/Users/vawsome/proj/rust/rust/src'

if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif

call plug#end()
filetype plugin indent on

" -----------------------------------------------------------------------------
" General
" -----------------------------------------------------------------------------

let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git\|_build'

imap jj <Esc>

filetype plugin indent on
let maplocalleader = ","
let g:maplocalleader = ","
set history=1000
syntax enable
set autoread

" -----------------------------------------------------------------------------
" Russian
" -----------------------------------------------------------------------------
" switch to russian layout by c-^ in insert mode
set keymap=russian-jcukenmac
set iminsert=0
set imsearch=0

" -----------------------------------------------------------------------------
" UI
" -----------------------------------------------------------------------------

set title
set encoding=utf-8
set scrolloff=3
set autoindent
set smartindent
set showmode
set showcmd
set hidden
set wildmenu
set wildmode=list:longest
set novisualbell
set cursorline
set ttyfast
set ruler
set backspace=indent,eol,start
set laststatus=2
set number
" set relativenumber
set undofile

" Auto adjust window sizes when they become current
set winwidth=84
set winheight=5
set winminheight=5
set winheight=999

" colorscheme darkburn
colorscheme solarized

let dark_theme = matchstr($ITERM_PROFILE, 'dark')
if empty(dark_theme)
  set background=light
else
  set background=dark
endif

set t_Co=256

set foldmethod=indent
set nofoldenable

set mouse=a
set mousemodel=popup

" ---------------------------------------------------------------------------
" Text Formatting
" ---------------------------------------------------------------------------
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab

set nowrap
set textwidth=119
set formatoptions=n
set colorcolumn=120

" ---------------------------------------------------------------------------
" Deoplete
" ---------------------------------------------------------------------------
let g:deoplete#enable_at_startup = 1
let g:deoplete#auto_complete_delay = 0
let g:deoplete#enable_ignore_case = 1
let g:deoplete#enable_smart_case = 1
let g:deoplete#auto_complete_start_length = 2
set completeopt-=preview

" ---------------------------------------------------------------------------
" Status Line
" ---------------------------------------------------------------------------
let g:airline_powerline_fonts = 1
let g:airline#extensions#keymap#enabled = 0
" path
set statusline=%f
" flags
set statusline+=%m%r%h%w
" git branch
set statusline+=\ %{fugitive#statusline()}
" encoding
set statusline+=\ [%{strlen(&fenc)?&fenc:&enc}]
" line x of y
set statusline+=\ [line\ %l\/%L]
" Syntax
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

" ---------------------------------------------------------------------------
" Font & Icons
" ---------------------------------------------------------------------------
" brew tap caskroom/fonts
" brew cask install font-hack-nerd-font
" use Hack Nerd Font

" Colour
hi StatusLine ctermfg=Black ctermbg=White

" Change colour of statusline in insert mode
au InsertEnter * hi StatusLine ctermbg=DarkBlue
au InsertLeave * hi StatusLine ctermfg=Black ctermbg=White

" ---------------------------------------------------------------------------
" Syntastic
" ---------------------------------------------------------------------------
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" ---------------------------------------------------------------------------

set ignorecase
set smartcase
set gdefault
set incsearch
set showmatch
set hlsearch
" turn search highlight off
nnoremap <leader><space> :noh<cr>

" Center screen when scrolling search results
nmap n nzz
nmap N Nzz

imap <C-h> <ESC>^i
imap <C-l> <ESC>$a

" ACK
" let g:ackprg = 'ag --vimgrep --ignore-dir=public --ignore-dir=priv --ignore-dir=log --ignore-dir=deps --ignore-dir=_build --ignore-dir=node_modules'
" let g:ag_prg="ag --vimgrep"

" Auto format
map === mmgg=G`m^zz

" close buffer
nmap <leader>d :bd<CR>

" Switch between last two buffers
nnoremap <leader><leader> <c-^>
inoremap <leader><leader> <esc><c-^>
vnoremap <leader><leader> <esc><c-^>

" Edit/View files relative to current directory
cnoremap %% <C-R>=expand('%:h').'/'<cr>
map <leader>e :edit %%
map <leader>v :view %%

" Ignore some binary, versioning and backup files when auto-completing
set wildignore=.svn,CVS,.git,*.swp,*.jpg,*.png,*.gif,*.pdf,*.bak
" Set a lower priority for .old files
set suffixes+=.old

set pastetoggle=<F5>

" ---------------------------------------------------------------------------
" Function Keys
" ---------------------------------------------------------------------------

imap <F2> <esc>:w<CR>
vmap <F2> <esc>:w<CR>
nmap <F2> :w<CR>

map <F3> :ConqueTerm bash<CR>
nnoremap <F4> :GundoToggle<CR>

imap <leader>/ <esc>:BufExplorer<CR>
vmap <leader>/ <esc>:BufExplorer<CR>
nmap <leader>/ :BufExplorer<CR>

" ---------------------------------------------------------------------------
" Plugins
" ---------------------------------------------------------------------------

" FZF
nnoremap <leader>b :Buffers<CR>
nnoremap <leader>a :Rg<space>
nnoremap <leader>t :BTags<CR>
nnoremap <leader>f :Files<CR>
nnoremap <leader>h :History<CR>
nmap */ :Rg \b<C-R><C-W>\b

" NERDTree
let NERDTreeShowBookmarks = 0
let NERDChristmasTree = 1
let NERDTreeWinPos = "left"
let NERDTreeHijackNetrw = 1
let NERDTreeQuitOnOpen = 1
let NERDTreeWinSize = 50
" open file browser
map <leader>p :NERDTreeToggle<cr>

" TagList
set tags=./tags;
set tags+=gems.vtags
set tags+=ruby.vtags
map <leader>l :TlistToggle <cr>
let Tlist_Use_Right_Window = 1
" let Tlist_WinWidth = 60
let Tlist_Show_One_File = 1
let Tlist_Close_On_Select = 1
let Tlist_Exit_OnlyWindow = 1
let Tlist_Sort_Type = "name"
let Tlist_Compact_Format = 1
let Tlist_GainFocus_On_ToggleOpen = 1
let Tlist_Enable_Fold_Column      = 0

" Use only current file to autocomplete from tags
" set complete=.,t
set complete=.,w,b,u,t,i

" Find in NERDTree
nmap <silent> <leader>[ :NERDTreeFind<CR>

" AutoClose
let g:AutoClosePairs = {'(': ')', '{': '}', '[': ']', '"': '"', "'": "'", '#{': '}'}
let g:AutoCloseProtectedRegions = ["Character"]

let my_home = expand("$HOME/")

if filereadable(my_home . '.vim/bundle/vim-autocorrect/autocorrect.vim')
  source ~/.vim/bundle/vim-autocorrect/autocorrect.vim
endif

" Blame
nmap <Leader>gb :Gblame<CR>

" Tabularize
if exists(":Tab")
  nmap <leader>a\| :Tab /\|<CR>
  vmap <leader>a\| :Tab /\|<CR>
  nmap <leader>a= :Tab /=<CR>
  vmap <leader>a= :Tab /=<CR>
  nmap <leader>a: :Tab /:\zs<CR>
  vmap <leader>a: :Tab /:\zs<CR>
endif

let g:cssColorVimDoNotMessMyUpdatetime = 1

" Easy commenting
nnoremap // :TComment<CR>
vnoremap // :TComment<CR>

" Supertab
let g:SuperTabCrMapping = 0
let g:SuperTabDefaultCompletionType = "<c-x><c-o>"
let g:SuperTabContextDefaultCompletionType = "<c-x><c-o>"
" let g:SuperTabNoCompleteBefore (default value: [])
" let g:SuperTabNoCompleteAfter (default value: ['^', '\s'])

" Syntastic
" let g:syntastic_auto_loc_list=1
" let g:syntastic_auto_jump=1

" ---------------------------------------------------------------------------
" C++
" ---------------------------------------------------------------------------

let g:deoplete#sources#clang#libclang_path = "/Library/Developer/CommandLineTools/usr/lib/libclang.dylib"
let g:clang_library_path = "/Library/Developer/CommandLineTools/usr/lib/libclang.dylib"

" ---------------------------------------------------------------------------
" Ruby/Rails
" ---------------------------------------------------------------------------

" Other files to consider Ruby
au BufRead,BufNewFile Gemfile,Rakefile,Thorfile,config.ru,Vagrantfile,Guardfile,Capfile set ft=ruby

" ---------------------------------------------------------------------------
" CoffeeScript
" ---------------------------------------------------------------------------

let coffee_compile_vert = 1
au BufNewFile,BufReadPost *.coffee setl foldmethod=indent

" ---------------------------------------------------------------------------
" SASS / SCSS
" ---------------------------------------------------------------------------

au BufRead,BufNewFile *.haml set filetype=haml
au BufNewFile,BufReadPost *.scss setl foldmethod=indent
au BufNewFile,BufReadPost *.sass setl foldmethod=indent
au BufRead,BufNewFile *.scss set filetype=scss

" ---------------------------------------------------------------------------
" GUI
" ---------------------------------------------------------------------------

if has("gui_running")
  set guioptions-=T " no toolbar set guioptions-=m " no menus
  set guioptions-=r " no scrollbar on the right
  set guioptions-=R " no scrollbar on the right
  set guioptions-=l " no scrollbar on the left
  set guioptions-=b " no scrollbar on the bottom
  set guioptions=aiA
  set mouse=v
  set guifont=Hack\ Nerd\ Font\:h12
endif

" ---------------------------------------------------------------------------
" Directories
" ---------------------------------------------------------------------------

set backupdir=~/.vim/.tmp,~/tmp,/tmp
set dir=~/.vim/.tmp,~/tmp,/tmp
set undodir=~/.vim/.tmp,~/tmp,/tmp

" Ctags path (brew install ctags)
let Tlist_Ctags_Cmd = 'ctags'

" Finally, load custom configs
if filereadable(my_home . '.vimrc.local')
  source ~/.vimrc.local
endif

" ---------------------------------------------------------------------------
" Misc
" ---------------------------------------------------------------------------

" When vimrc, either directly or via symlink, is edited, automatically reload it
autocmd! bufwritepost .vimrc source %
autocmd! bufwritepost vimrc source %


" Everytime the user issue a :w command, Vim will automatically remove all trailing whitespace before saving.
autocmd BufWritePre * :%s/\s\+$//e

